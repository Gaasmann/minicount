/*
 *  Minicount, a small refund application
 *  Copyright (C) 2024 Jérôme Jutteau <jerome _ÀT_ jutteau _DOT fr>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Hi! If you look here, that's you might be interested to know how it works.
 *
 * This algorithm tries to minimise the number of money exchange.
 *
 * Phase 1: build the balance of each people (positive or negative).
 * A positive balance indicate that the person need to give money.
 * A negative balance indicate that the person need to receive money.
 * The total of all balances should be zero.
 *
 * Phase 2: try to minimize the exchanges until all balances are zero.
 * Steps:
 * 0. have a list of all remaining balances
 * 1. have a list of refunds (empty)
 * 2. take the highest balance and lowest balance
 * 3. the balance which have the highest absolute value is 'A'
 * 4. the balance which have the lowest absolute value is 'B'
 * 5. add refund from the positive to the negative balance of value abs(B)
 * 6. 'A' balance value is now A + B
 * 7. remove A from remaining balances if A is equal to 0
 * 8. B balance is now 0, remove B from remaining balances
 * 9. go to step 2 until remaining balances list is empty
 *
 * In "complex_2" test, I optained the same results as Tricount app.
 */

#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
use serde_yaml as yml;
use std::collections::HashMap;
use std::env;
use std::f32;
use std::fs::File;
use std::io::prelude::*;
use std::process::exit;

fn main() {
    let paths = env::args().skip(1).collect::<Vec<String>>();
    if paths.is_empty() {
        print_man();
        exit(0);
    }
    let bills = bills_build(paths);
    bills_print(&bills);
    let balances = balances_build(&bills);
    balances_print(&balances);
    let refunds = refunds_build(&balances);
    refunds_print(&refunds);
}

fn print_man() {
    const VERSION: Option<&str> = option_env!("CARGO_PKG_VERSION");
    println!(
        "minicount v{} - a small refund application",
        VERSION.unwrap_or("?")
    );
    println!("Usage: minicount FILE [FILE]...");
}

#[derive(Deserialize, Debug)]
struct Bill {
    // Who paid
    who: String,
    // What has been paid
    what: String,
    // Price of it
    price: f32,
    // For which participants. None => everybody
    #[serde(rename = "for")]
    for_: Option<Vec<String>>,
}

fn bills_build(paths: Vec<String>) -> Vec<Bill> {
    let mut bills = Vec::new();
    for path in paths {
        let mut file = File::open(&path).unwrap_or_else(|_e| {
            eprintln!("Cannot open file \"{}\"", path);
            exit(1);
        });
        let mut content = String::new();
        file.read_to_string(&mut content).unwrap_or_else(|_e| {
            eprintln!("Cannot read file \"{}\"", path);
            exit(1);
        });
        let mut file_bills: Vec<Bill> = yml::from_str(content.as_str()).unwrap_or_else(|_e| {
            eprintln!("Invalid expected format in file \"{}\"", path);
            exit(1);
        });
        bills.append(&mut file_bills);
    }
    if bills.is_empty() {
        eprintln!("No bill found");
        exit(1);
    }
    bills
}

fn bills_print(bills: &[Bill]) {
    println!("# Bills");
    for b in bills.iter() {
        print!("- {} paid {:.2} ({} for ", b.who, b.price, b.what);
        match b.for_ {
            Some(ref list) => print_comma_separated_string(list),
            None => print!("everybody"),
        };
        println!(")")
    }
    println!();
}

fn print_comma_separated_string(list: &[String]) {
    let mut it = list.iter();
    if let Some(str) = it.next() {
        print!("{}", str);
        for str in it {
            print!(", {}", str);
        }
    }
}

type Balances = HashMap<String, f32>;

fn balances_build(bills: &[Bill]) -> Balances {
    let mut balances = Balances::new();
    // Init all balances to zero
    for bill in bills.iter() {
        balances.insert(bill.who.clone(), 0f32);
        if let Some(ref for_) = bill.for_ {
            for f in for_.iter() {
                balances.insert(f.clone(), 0f32);
            }
        }
    }

    // Set balances
    for bill in bills.iter() {
        // Decrement balance of payer
        if let Some(balance) = balances.get_mut(&bill.who) {
            *balance -= bill.price;
        }

        // Increment balance of beneficiaries list
        let mut for_everybody = false;
        if let Some(people) = &bill.for_ {
            if people.is_empty() {
                for_everybody = true;
            } else {
                for pers in people.iter() {
                    let div = bill.price / people.len() as f32;
                    if let Some(balance) = balances.get_mut(pers) {
                        *balance += div;
                    }
                }
            }
        } else {
            for_everybody = true;
        }
        if for_everybody {
            // Increment balance of everybody
            let div = bill.price / balances.len() as f32;
            for balance in balances.values_mut() {
                *balance += div;
            }
        }
    }
    balances
}

fn balances_print(balances: &Balances) {
    println!("# Balances");
    for (name, balance) in balances.iter() {
        if *balance >= 0f32 {
            println!("- {}: +{:.2}", name, balance);
        } else {
            println!("- {}: {:.2}", name, balance);
        }
    }
    println!();
}

struct Refund {
    from: String,
    to: String,
    value: f32,
}

fn refunds_build(balances: &Balances) -> Vec<Refund> {
    let mut refunds = Vec::new();
    let mut remaining = balances.clone();
    while remaining.len() > 1 {
        // Get minimal balance
        let mut min_name = String::new();
        let mut min_value = 0f32;
        for (name, value) in remaining.iter() {
            if *value < min_value {
                min_name = name.clone();
                min_value = *value;
            }
        }
        // Get maximal balance
        let mut max_name = String::new();
        let mut max_value = 0f32;
        for (name, value) in remaining.iter() {
            if *value > max_value {
                max_name = name.clone();
                max_value = *value;
            }
        }
        // Looking for the highest abs value
        let min_value_abs = min_value.abs();
        let max_value_abs = max_value.abs();
        let (a_name, b_name, value) = match min_value_abs < max_value_abs {
            true => (max_name.clone(), min_name.clone(), min_value_abs),
            false => (min_name.clone(), max_name.clone(), max_value_abs),
        };
        // Update balance
        let new_a_value = min_value + max_value;
        if is_zero(new_a_value) {
            remaining.remove(&a_name);
        } else {
            remaining.insert(a_name, new_a_value);
        }
        remaining.remove(&b_name);
        // Add refund
        refunds.push(Refund {
            from: max_name,
            to: min_name,
            value,
        });
    }
    refunds
}

fn refunds_print(refunds: &[Refund]) {
    println!("# Refunds");
    for r in refunds.iter() {
        println!("- {} pays {:.2} to {}", r.from, r.value, r.to);
    }
}

fn is_zero(f: f32) -> bool {
    -0.001 < f && f < 0.001
}

#[cfg(test)]
mod tests {
    use super::*;

    fn balances_sum_zero(balances: &Balances) -> bool {
        let mut sum = 0f32;
        for b in balances.values() {
            sum += *b;
        }
        is_zero(sum)
    }

    #[test]
    fn zero() {
        let bills = Vec::new();
        let balances = balances_build(&bills);
        assert_eq!(balances.len(), 0);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 0);
    }

    #[test]
    fn alone() {
        let bills = vec![Bill {
            who: String::from("A"),
            what: String::from("x"),
            price: 10f32,
            for_: None,
        }];
        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 1);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 0);
    }

    #[test]
    fn one() {
        let bills = vec![Bill {
            who: String::from("A"),
            what: String::from("x"),
            price: 10f32,
            for_: Some(vec![String::from("B")]),
        }];
        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 2);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 1);
    }

    #[test]
    fn two_1() {
        let bills = vec![
            Bill {
                who: String::from("A"),
                what: String::from("x"),
                price: 10f32,
                for_: Some(vec![String::from("B")]),
            },
            Bill {
                who: String::from("B"),
                what: String::from("x"),
                price: 12f32,
                for_: Some(vec![String::from("A")]),
            },
        ];

        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 2);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 1);
    }

    #[test]
    fn two_2() {
        let bills = vec![
            Bill {
                who: String::from("A"),
                what: String::from("x"),
                price: 10f32,
                for_: None,
            },
            Bill {
                who: String::from("B"),
                what: String::from("x"),
                price: 12f32,
                for_: None,
            },
        ];

        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 2);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 1);
    }

    #[test]
    fn two_2_empty_for_list() {
        let bills = vec![
            Bill {
                who: String::from("A"),
                what: String::from("x"),
                price: 10f32,
                for_: Some(vec![]),
            },
            Bill {
                who: String::from("B"),
                what: String::from("x"),
                price: 12f32,
                for_: Some(vec![]),
            },
        ];

        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 2);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 1);
    }

    #[test]
    fn complex_1() {
        let yaml = String::from(
            "
            - who: Alice
              price: 35
              what: pizza
              for:
                  - Alice
                  - Bob
            - who: Alice
              price: 100
              what: sunday's restaurant
            - who: Charly
              price: 53
              what: books
              for: [Bob]
            - who: Bob
              price: 35
              what: ice creams
              for: [Alice, Charly]
        ",
        );
        let bills: Vec<Bill> = yml::from_str(yaml.as_str()).unwrap();
        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 3);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 2);
    }

    #[test]
    fn complex_2() {
        let yaml = String::from(
            "
            - who: Pa
              price: 49.40
              what: Es
              for: [Al, Ca, Ga, Ja, Mo, Ms, Pa, Pi, Ro]
            - who: Ro
              price: 79.40
              what: Es
              for: [Ca, Ga, Ja, Mo, Ms, Pa, Pi, Ro]
            - who: Ga
              price: 49.40
              what: Es
              for: [Ca, Ga, Ja, Mo, Ms, Pa, Pi, Ro]
            - who: Pi
              price: 57
              what: Mag
            - who: Ga
              price: 4.80
              what: Pe
              for: [Ms, Pa, Pi, Ro]
            - who: Ja
              price: 4.80
              what: Pe
              for: [Ca, Ga, Ja, Mo]
            - who: Ro
              price: 286.50
              what: Ka
              for: [Ca, Ga, Ja, Mo, Ms, Pa, Pi, Ro]
            - who: Ja
              price: 288.26
              what: Bo
              for: [Ja, Mo, Ms, Pi, Ro]
            - who: Ja
              price: 52.40
              what: Bo
              for: [Ca, Ga]
            - who: Ja
              price: 13
              what: Bo
              for: [Al]
            - who: Ca
              price: 37.40
              what: Pe
            - who: Ro
              price: 15
              what: Ti
            - who: Ja
              price: 38.02
              what: Je
              for: [Al, Ja, Mo, Ms, Pa, Pi, Ro]
            - who: Ro
              price: 14.80
              what: KJ
            - who: Ja
              price: 19.39
              what: kd
            - who: Ro
              price: 37.40
              what: Pe
            - who: Pa
              price: 37.40
              what: Pe
            - who: Ja
              price: 133
              what: Ac
              for: [Ca, Ga, Ja, Mo, Ms, Pa, Pi, Ro]
        ",
        );
        let bills: Vec<Bill> = yml::from_str(yaml.as_str()).unwrap();
        let balances = balances_build(&bills);
        assert!(balances_sum_zero(&balances));
        assert_eq!(balances.len(), 9);
        let refunds = refunds_build(&balances);
        assert_eq!(refunds.len(), 8);
    }
}
