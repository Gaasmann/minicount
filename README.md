# Minicount, a small refund application

[Minicount](https://gitlab.com/mojo42/minicount) is a console-based, Libre Software clone of Tricount application.

It's goal is to minimize the number of repayments in a group of people in a [Keep It Simple Stupid way](https://en.wikipedia.org/wiki/KISS_principle): no mobile app, no database, no server.

Describe in a yaml file your expense and let minicount outputs the details.

Check `example.yml` file to understand the syntax and make your own.

# Invocation

Tricount basically transform a yaml file like this:
```yaml
$cat example.yml
# In this example, we have 3 people: Alice, Bob and Charly
---
# Alice paid a pizza for Bob (and for herself)
- who: Alice
  price: 35
  what: pizza
  for:
      - Alice
      - Bob
# Alice paid a restaurant to eveybody, no need to add all names
- who: Alice
  price: 100
  what: sunday's restaurant
# Charly paid some books for Bob
- who: Charly
  price: 53
  what: books
  for: [Bob]
# Bob paid ice creams to Charly and Alice
- who: Bob
  price: 35
  what: ice creams
  for: [Alice, Charly]
```

and transform it into text report you can send to your friends:
```
$ minicount example.yml

# Bills
- Alice paid 35.00 (pizza for Alice, Bob)
- Alice paid 100.00 (sunday's restaurant for everybody)
- Charly paid 53.00 (books for Bob)
- Bob paid 35.00 (ice creams for Alice, Charly)

# Balances
- Bob: +68.83
- Charly: -2.17
- Alice: -66.67

# Refunds
- Bob pays 66.67 to Alice
- Bob pays 2.17 to Charly
```

# Method

A full explanation is available in sources (see `src/main.rs`).

I got the same results as Tricount application for a complex example so I suppose that's the same algorithm.

# Install instructions

```bash
$ cargo install minicount
```

# Build instructions

You will need to install [Rustlang](https://www.rust-lang.org/) and then build the project:
```bash
$ cargo build --release
```
You will then find the program in `target/release/minicount`.

# License

This whole project is licensed under GNU Affero General Public License v3 (AGPL-3.0).

The GNU Affero General Public License can be found at https://www.gnu.org/licenses/agpl.html.

For an overview of AGPL-3.0, check [tldrlegal](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).
